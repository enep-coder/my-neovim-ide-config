# My NeoVim IDE Config

May Neovim and Neovide configuration for Python, HTML/CSS/JavaScript, PHP development

## Screenshots

![Screenshot Neovide](neovide.png "Screenshot NeoVide")
Screenshot my neovide в Manjaro Linux KDE

## Plugins used
1. [wbthomason/packer.nvim](https://github.com/wbthomason/packer.nvim) - Plugin Manager
2. [EdenEast/nightfox.nvim](https://github.com/EdenEast/nightfox.nvim) - Theme set Nightfox
3. [akinsho/bufferline.nvim](https://github.com/akinsho/bufferline.nvim) - Tabbar buffer
4. [kyazdani42/nvim-web-devicons]()

## coc.nvim extentions
1. [coc.pyright](https://github.com/fannheyward/coc-pyright) https://github.com/fannheyward/coc-pyright - for python 3
2. [coc-tsserver](https://github.com/neoclide/coc-tsserver) - for JavaScript
3. [coc-lua](https://github.com/josa42/coc-lua) - for Lua
4. [coc-html](https://github.com/neoclide/coc-html) - for HTML]
5. [coc-css](https://github.com/neoclide/coc-css) - for CSS
6. [coc-emmet](https://github.com/neoclide/coc-emmet) - for emmet