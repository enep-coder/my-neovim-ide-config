-- Plugins
vim.cmd [[packadd packer.nvim]]
packer = require('packer')
packer.startup({function()
    use 'wbthomason/packer.nvim'
    use 'EdenEast/nightfox.nvim'
    use 'akinsho/bufferline.nvim'
    use 'kyazdani42/nvim-web-devicons'
--    use 'kyazdani42/nvim-tree.lua'
    -- Навигация внутри файла по классам и функциям
    use 'majutsushi/tagbar'
    use {'neoclide/coc.nvim', branch = 'master', run = 'yarn install --frozen-lockfile'}
    -- Даже если включена русская раскладка vim команды будут работать
    use 'powerman/vim-plugin-ruscmd'
    use { 'nvim-lualine/lualine.nvim',
        requires = { 'kyazdani42/nvim-web-devicons', opt = true
        }
    }
    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.0',
        -- or                            , branch = '0.1.x',
        requires = { {'nvim-lua/plenary.nvim'} }
    }
    use {'nvim-telescope/telescope-fzf-native.nvim', run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }
    use {
        'kyazdani42/nvim-tree.lua',
        tag = 'nightly' -- optional, updated every week. (see issue #1193)
    }
    use {"akinsho/toggleterm.nvim", tag = 'v2.*'}
end,
config = {
  display = {
    open_fn = function()
      return require('packer.util').float({ border = 'single' })
    end
  }
}})

