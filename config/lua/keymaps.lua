local map = vim.api.nvim_set_keymap
local default_opts = {noremap = true, silent = true}

map('n','<C-s>',':w<CR>',default_opts)
map('i','<C-s>','<Esc>:w<CR><i>',default_opts)
map('n','<C-t><CR>',':ToggleTerm<CR>',default_opts)
map('i','<C-t><CR>','<Esc>:ToggleTerm<CR><i>',default_opts)
map('n','<C-o>',':NvimTreeToggle<CR>',default_opts)
map('n', '<Tab>', ':BufferLineCycleNext<CR>', default_opts)
map('n', '<S-Tab>', ':BufferLineCyclePrev<CR>', default_opts)
map('n', '<F8>',':TagBarToggle<CR>',default_opts)
map('n','<C-u>',':Undo<CR>',default_opts)
map('n','<C-r>',':redo<CR>',default_opts)
map('n', '<F5>', ':exec &nu==&rnu? "se nu!" : "se rnu!"<CR>', default_opts)
--map('n', '<S-F3>', ':e ~/.config/nvim/init.lua<CR>:e ~/.config/nvim/lua/plugins.lua<CR>:e ~/.config/nvim/lua/settings.lua<CR>:e ~/.config/nvim/lua/keymaps.lua<CR>', defaults_opts)
--map('n', '<F4>',':so ~/.config/nvim/init.lua',defaults_opts)




