local cmd = vim.cmd
local exec = vim.api.nvim_exec
local vim = vim

local g = vim.g
local opt = vim.opt


-- Main settings
cmd([[
if exists("g:neovide")
    set guifont=Hack:h9
endif
]])
--opt.guifont = {"Hack", "h10"}
opt.mouse = 'a'
opt.number = true
-- Цветавая схема
opt.termguicolors = true
-- Табы и отсупы
opt.shiftwidth = 4
opt.smartindent = true
opt.termguicolors = true

cmd([[

filetype indent plugin on
syntax enable
colorscheme nightfox
]])

vim.o.clipboard = "unnamedplus"
-- Настройки плагинов

g.tagbar_compact = 1
g.tagbar_sort = 0

require('lualine').setup{
    options = {
        theme='nightfox'
    }
}
require('bufferline').setup {
  options = {
    mode = "buffers", -- set to "tabs" to only show tabpages instead
    numbers = "buffer_id" ,
    close_command = "bdelete! %d",       -- can be a string | function, see "Mouse actions"
    right_mouse_command = "bdelete! %d", -- can be a string | function, see "Mouse actions"
    left_mouse_command = "buffer %d",    -- can be a string | function, see "Mouse actions"
    middle_mouse_command = nil,          -- can be a string | function, see "Mouse actions"
    indicator_icon = '▎',
    buffer_close_icon = '',
    modified_icon = '●',
    close_icon = '',
    left_trunc_marker = '',
    right_trunc_marker = '',
    name_formatter = function(buf)  -- buf contains a "name", "path" and "bufnr"
      -- remove extension from markdown files for example
      if buf.name:match('%.md') then
        return vim.fn.fnamemodify(buf.name, ':t:r')
      end
    end,
    max_name_length = 18,
    max_prefix_length = 15, -- prefix used when a buffer is de-duplicated
    tab_size = 18,
    diagnostics =  "coc",
    diagnostics_update_in_insert = false,

      -- filter out based on arbitrary rules
      -- e.g. filter out vim wiki buffer from tabline in your work repo

    offsets = {{filetype = "NvimTree", text = "File Explorer"},{filetype="tagbar", text="Tag Explorer"}},
    color_icons = true, -- whether or not to add the filetype icon highlights
    show_buffer_icons = true, -- disable filetype icons for buffers
    show_buffer_close_icons = true,
    show_buffer_default_icon = true, -- whether or not an unrecognised filetype should show a default icon
    show_close_icon = true,
    show_tab_indicators = true,
    persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
    -- can also be a table containing 2 custom separators
    -- [focused and unfocused]. eg: { '|', '|' }
    separator_style = "slant",
    enforce_regular_tabs = false,
    always_show_bufferline = true ,
    sort_by = 'id'
  }
}
require("nvim-tree").setup({
  sort_by = "case_sensitive",
  view = {
    adaptive_size = true,
    mappings = {
      list = {
        { key = "u", action = "dir_up" },
      },
    },
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
})
require("toggleterm").setup()
