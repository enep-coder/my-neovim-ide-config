#!/usr/bin/bash
NVIM="/usr/bin/nvim"
NEOVIDE="/usr/bin/neovide"

NVIM_CONFIG_DIR="${HOME}/.config/nvim"
NVIM_LOCAL_DIR="${HOME}/.local/nvim"
NVIM_COC_DIR="${HGME}/.config/coc"
ERROR=0

printf "Install configuration files for neovim/neovide\n"
printf "==============================================\n"

printf "\tCheck Neovim/Neovide application ..."
if [ -x ${NVIN} ] ||  [ -x ${NEOVIDE} ]; then
    printf " The neovim/neovide installed.\n"
    ERROR=0
else
    printf " Please install Neovim/Neovide. \n"
    ERROR=-1
    
fi
printf "Exit Installetion script ("
if [ ${ERROR} == 0 ] 
then 
    printf "OK!).\n"
else 
    printf "FAIL!).\n"
fi
exit ${ERROR}
    
